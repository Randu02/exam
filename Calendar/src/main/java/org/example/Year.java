package org.example;

public class Year {
    public static void main(String args[]){
        int Y = 2019;    // year
        int startDayOfMonth = 2;
        int spaces = startDayOfMonth;

        // months[i] = name of month i
        String[] months = {
                "",                               // leave empty so that we start with months[1] = "January"
                "January", "February", "March",
                "April", "May", "June",
                "July", "August", "September",
                "October", "November", "December"
        };

        // days[i] = number of days in month i
        int[] days = {
                0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
        };

        for (int M = 1; M <= 12; M++) {

            // check for leap year
            if  ((((Y % 4 == 0) && (Y % 100 != 0)) ||  (Y % 400 == 0)) && M == 2)
                days[M] = 29;


            // print calendar header
            // Display the month and year
            System.out.println("=========Bulan "+ months[M] + "========");
            System.out.println();

            // Display the lines

            System.out.println("  M   S   S   R   K   J   S");

            // spaces required
            spaces = (days[M-1] + spaces)%7;

            // print the calendar
            for (int i = 0; i < spaces; i++)
                System.out.print(" -- ");
            for (int i = 1; i <= days[M]; i++) {
                System.out.printf(" %2d ", i);
                if (((i + spaces) % 7 == 0) || (i == days[M])) System.out.println("");
            }
            System.out.println(" --  --  --  --  --  --  -- ");
            System.out.println("=============================");
        }
    }

}
